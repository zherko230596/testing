import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Text, SafeAreaView, StyleSheet, View, Pressable, TextInput, FlatList } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeScreenContainer } from './src/connectedScreens/HomeScreenContainer';
import { Provider } from 'react-redux';
import {store, persistor} from './redux/store'
import { PersistGate } from 'redux-persist/integration/react';
import { DetailsScreenContainer } from './src/connectedScreens/DetailsScreenContainer';

import { HomeScreen } from './src/screens/HomeScreen';

const Stack = createNativeStackNavigator();

const App =  () => {
	return (
		<Provider store = {store}>
			<PersistGate loading={null} persistor={persistor}>
				<NavigationContainer>
					<Stack.Navigator screenOptions={() => ({headerShown: false})}>
						<Stack.Screen name="Home" component={HomeScreenContainer} />
						<Stack.Screen name="Details" component={DetailsScreenContainer} />
					</Stack.Navigator>
				</NavigationContainer>
			</PersistGate>
		</Provider>
	);
};

const styles = StyleSheet.create({
	button:{
		marginTop: 40,
		width: '75%',
		height: 50,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
	},
	button1:{
		marginTop: 20,
		width: 60,
		height: 50,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginRight: 24,
		marginLeft: 24,
		borderWidth: 2,
		borderColor: 'grey'
	},
	item: {
		backgroundColor: '#f9c2ff',
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	  },
	  title: {
		fontSize: 32,
	  },
});

export default App;