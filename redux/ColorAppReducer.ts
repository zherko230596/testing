import { StackRouter } from "@react-navigation/native"

const SET_COLOR_APP = 'SET_COLOR_APP'
const RISE_COUNTER = 'RISE_COUNTER'
const REDUCE_COUNTER = 'REDUCE_COUNTER'
const ID = 'ID'
const TEXT_NOTE = 'TEXT_NOTE'
const ADD_TODO = 'ADD_TODO'

const initialState = {
    colorApp: 'yellow',
    counterApp: 0,
    id: 0,
    data : [
        {
            id: 1,
            title: 'first item',
        },
        {
            id: 2,
            title: 'second item',
        },
        {
            id: 3,
            title: 'third item',
        },
    ],
}

const ColorAppReducer = (state = initialState, action: any) => {
    switch(action.type) {
        case SET_COLOR_APP: 
            return{
                ...state,
                colorApp: action.color
            }
        case RISE_COUNTER: 
            return{
                ...state,
                counterApp: state.counterApp + 1
            }
        case REDUCE_COUNTER: 
            return{
                ...state,
                counterApp: state.counterApp - 1
            }
        case ID:
            return{
                ...state,
                id: state.id + 1
            }
        case TEXT_NOTE: 
            return{
                ...state,
                textNote: action.text
            }
        case ADD_TODO:
            let copyTODO = [...state.data]
            copyTODO.push({id : state.data[state.data.length - 1].id + 1, title: action.title})
            return{
                ...state,
                data: copyTODO
            }
        default: 
            return state;
    }
}

export const setColorActionCreator = (color: string) =>({type: SET_COLOR_APP, color})
export const riseCounterActionCreator = () =>({type: RISE_COUNTER})
export const reduceCounterActionCreator = () =>({type: REDUCE_COUNTER})
export const setIDListToDo = () =>({type: ID})
export const setTextNote = (text: string) =>({type: SET_COLOR_APP, text})
export const addNewTODO = (title: string) =>({type: ADD_TODO, title})

export default ColorAppReducer