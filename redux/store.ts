// import { AsyncStorage } from 'redux-persist';
//конфигурационный файл для редакс стора
import { combineReducers, createStore } from "redux";
import  ColorAppReducer  from './ColorAppReducer'
import { persistReducer, persistStore } from "redux-persist";
import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['data']
}

const reducers = combineReducers({
    app: persistReducer(persistConfig, ColorAppReducer)
})

//const persistedReducer = persistReducer(persistConfig, reducers)

// export default () => {
//     let store = createStore(reducers);
//     let persistor = persistStore(store)
//     return {store, persistor}
// }

export const store = createStore(reducers);
export const persistor = persistStore(store);
