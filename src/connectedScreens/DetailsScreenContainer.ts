import { setColorActionCreator, setIDListToDo, setTextNote, addNewTODO } from '../../redux/ColorAppReducer';
import { connect } from "react-redux";
import { DetailsScreen } from '../screens/DetailsScreen'; 
import { HomeScreen } from '../screens/HomeScreen';

const mapStateToProps = (state:any) => ({
    globalStore: state.app
})

export const DetailsScreenContainer = connect(mapStateToProps, {setColorActionCreator, setIDListToDo, setTextNote, addNewTODO})(DetailsScreen)