import { setColorActionCreator, reduceCounterActionCreator, riseCounterActionCreator } from '../../redux/ColorAppReducer';
import { connect } from "react-redux";
import { HomeScreen } from '../screens/HomeScreen';

const mapStateToProps = (state:any) => ({
    globalStore: state.app
})

export const HomeScreenContainer = connect(mapStateToProps, {setColorActionCreator, riseCounterActionCreator, reduceCounterActionCreator})(HomeScreen) 