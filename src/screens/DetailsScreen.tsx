import React, {useState} from "react";
import {View, Text, SafeAreaView, Pressable,StyleSheet, FlatList, TextInput, Button } from 'react-native'
import { getMovies } from "../API/api";


export const DetailsScreen = (props: any) => {
    const Item = ({ title }) => (
        <View style={styles.item}>
          <Text style={styles.title}>{title}</Text>
        </View>
    );
	const [text, setText] = useState('');

	const renderItem = ({ item }) => (
		<Item title = {item.title} />
	);
	const addNewParam = () => {
		props.addNewTODO(text)
		setText('')
	}
    const titleMap = async() => {
        const secondArray =  await getMovies();
        const final = secondArray.map((item: any) => item.title);
        console.log(final)
    }
	return (
		<SafeAreaView style = {{backgroundColor: props.globalStore.colorApp, height: '100%'}}>
			<Pressable onPress = {() => props.navigation.navigate('Home')}>
				<View style = {{marginTop: 24, alignItems: 'center'}}>
					<Text style = {{fontSize: 18}}> Your value is {props.globalStore.counterApp}</Text>
				</View>

				<View style = {{marginLeft: 24, marginRight: 24, marginTop: 40, borderRadius: 24, flexDirection: 'row', justifyContent: 'space-between'}}>
					<TextInput value = {text} style = {{fontSize: 20, borderBottomColor: 'black', borderBottomWidth: 2, width: '70%', paddingLeft: 12, paddingBottom: 4 }} placeholder='Give a title for note' onChangeText = {(text) => {setText(text)}}/>
					<Pressable style ={{width: '20%', marginLeft: '5%', backgroundColor: 'black', justifyContent: 'center', alignItems: 'center', borderRadius: 5}} onPress = {addNewParam}>
						<Text style = {{color: 'white', fontSize: 16}}>Add</Text>
					</Pressable>
				</View>
			</Pressable>
			<FlatList
				data={props.globalStore.data}
				renderItem={renderItem}
				keyExtractor={item => item.id}
			/>
            <Button title="Get response" onPress={ async() => {titleMap()}}/>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	button:{
		marginTop: 40,
		width: '75%',
		height: 50,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
	},
	button1:{
		marginTop: 20,
		width: 60,
		height: 50,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginRight: 24,
		marginLeft: 24,
		borderWidth: 2,
		borderColor: 'grey'
	},
	item: {
		backgroundColor: '#f9c2ff',
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	  },
	  title: {
		fontSize: 32,
	  },
});