import React, {useState} from "react";
import {View, Text, SafeAreaView, Pressable,StyleSheet, } from 'react-native'

export const HomeScreen = (props: any) => {
	const [text, setText] = React.useState('');
	const [isBgGreen, setGreen] = useState(false)
	const [isBgBlue, setBlue] = useState(false)
	const [isBgRed, setRed] = useState(false)

	const makeBgGreen = () => {
		// props.navigation.navigate('Home', {descr: text})
		setGreen(true)
		setBlue(false)
		setRed(false)
		props.setColorActionCreator('green')
	}
	const makeBgBlue = () => {
		// props.navigation.navigate('Home', {descr: text})
		props.setColorActionCreator('blue')
		setGreen(false)
		setBlue(true)
		setRed(false)
	}
	const makeBgRed = () => {
		// props.navigation.navigate('Home', {descr: text})
		setGreen(false)
		setBlue(false)
		setRed(true)
		props.setColorActionCreator('white')
	}
	const goNextScreen = () => {
		props.navigation.navigate('Details')
	}
	return (
		<SafeAreaView style = {[{backgroundColor:props.globalStore.colorApp, flex: 1, alignItems: 'center'}]}>
			<View style = {{flexDirection: 'row'}}>
				<Pressable  style = {[styles.button1, {backgroundColor: 'green'}]} onPress = {makeBgGreen} />
				<Pressable  style = {[styles.button1, {backgroundColor: 'blue'}]} onPress = {makeBgBlue} />
				<Pressable  style = {[styles.button1, {backgroundColor: 'white'}]} onPress = {makeBgRed} />
			</View>

			<Pressable  style = {[styles.button, {backgroundColor: 'black'}]} onPress = {goNextScreen}>
				<Text style = {{color: 'white', fontSize: 18, fontWeight: '700'}}>Next Screen</Text>
			</Pressable>

			<Pressable style = {{marginTop: 40}}>
				<Text style = {{fontSize: 18}}>Your counter is {props.globalStore.counterApp}</Text>
			</Pressable>

			<Pressable style = {[styles.button, {backgroundColor: 'green', marginTop: 24}]} onPress = {()=> props.riseCounterActionCreator()}>
				<Text style = {{color: 'white', fontSize: 18, fontWeight: '700'}}>Rise</Text>
			</Pressable>
			<Pressable style = {[styles.button, {backgroundColor: 'red', marginTop: 24}]} onPress = {()=> props.reduceCounterActionCreator()}>
				<Text style = {{color: 'white', fontSize: 18, fontWeight: '700'}}>Reduce</Text>
			</Pressable>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	button:{
		marginTop: 40,
		width: '75%',
		height: 50,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
	},
	button1:{
		marginTop: 20,
		width: 60,
		height: 50,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
		marginRight: 24,
		marginLeft: 24,
		borderWidth: 2,
		borderColor: 'grey'
	},
	item: {
		backgroundColor: '#f9c2ff',
		padding: 20,
		marginVertical: 8,
		marginHorizontal: 16,
	  },
	  title: {
		fontSize: 32,
	  },
});